package referme.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import referme.dto.AppUserDto;
import referme.dto.BaseMessage;
import referme.dto.FilterJobsRequest;
import referme.dto.GetDetailsResponse;
import referme.dto.GetJobsResponse;
import referme.dto.LoginResponse;
import referme.dto.PostJobsRequest;
import referme.dto.ReferMeRequest;
import referme.dto.RegistrationForm;
import referme.dto.RestPasswordRequest;
import referme.dto.UpdatePasswordRequest;
import referme.model.AppUser;
import referme.repositories.UserRepository;
import referme.security.SecurityUtils;
import referme.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserRepository userRepository;

//	@RequestMapping("/hello")
//	public String sayHello() {
//		return "hi referme";
//	}

//	@RequestMapping("/list")
//	public List<AppUser> getUserList() {
//		return this.userService.getUserList();
//	}

	// @PostMapping("/upload")
	// public BaseMessage singleFileUpload(@ModelAttribute RegistrationForm
	// registrationForm) {
	// MultipartFile file = registrationForm.getFile();
	// String fileName = file.getName();
	// String emailAdd = registrationForm.getEmail();
	// BaseMessage baseMessage = new BaseMessage();
	// if(file.isEmpty()) {
	// baseMessage.setStatus(false);
	// baseMessage.setMessage("Please select file to upload");
	// }
	// try {
	//
	// // Get the file and save it somewhere
	// File directory = new File(SecurityUtils.UPLOADED_FOLDER);
	// boolean isDirectoryExists = true;
	// if(!directory.exists()) {
	// isDirectoryExists = directory.mkdirs();
	// }
	// if(isDirectoryExists) {
	// File resume = new File(SecurityUtils.UPLOADED_FOLDER+emailAdd+".pdf");
	// file.transferTo(resume);
	// }
	//// byte[] bytes = file.getBytes();
	//// Path path = Paths.get(SecurityUtils.UPLOADED_FOLDER +File.separator+
	// fileName);
	//// Files.write(path, bytes);
	// baseMessage.setMessage("file uploaded successfully");
	// baseMessage.setStatus(true);
	//
	//
	// } catch (IOException e) {
	// baseMessage.setStatus(false);
	// baseMessage.setMessage(e.getMessage());
	// }
	// return baseMessage;
	// }

	@PostMapping("/sign-up")
	public BaseMessage signUp(@ModelAttribute RegistrationForm registrationForm) 
	{
		return this.userService.signUp(registrationForm);
		// res.addHeader(SecurityUtils.HEADER_STRING,
		// SecurityUtils.TOKEN_PREFIX + " " +
		// SecurityUtils.generateToken(user.getUserName()));
	}

	@PostMapping("/login")
	public LoginResponse login(@RequestBody AppUserDto user) 
	{
		LoginResponse loginResponse = new LoginResponse();
		if (user == null) 
		{
			loginResponse.setIsAuthenticated(false);
			loginResponse.setStatus(false);
			loginResponse.setMessage("User details is not provided");
		} 
		else if (user.getEmailAddress() == null) 
		{
			loginResponse.setIsAuthenticated(false);
			loginResponse.setStatus(false);
			loginResponse.setMessage("email-address is not provided");
		} 
		else if (user.getPassword() == null) 
		{
			loginResponse.setIsAuthenticated(false);
			loginResponse.setStatus(false);
			loginResponse.setMessage("password is not provided");
		}

		AppUser foundUser = userRepository.findByEmailAddress(user.getEmailAddress());

		if (foundUser == null) 
		{
			loginResponse.setIsAuthenticated(false);
			loginResponse.setStatus(false);
			loginResponse.setMessage("Email:" + user.getEmailAddress() + " is not registered, please sign-up");
		} 
		else 
		{
			if("N".equalsIgnoreCase(foundUser.getIsEmailVerified())) 
			{
				loginResponse.setIsAuthenticated(false);
				loginResponse.setStatus(false);
				loginResponse.setMessage("Email:" + user.getEmailAddress() + " is registered, But you haven't verified it. Please click on confirmation click sent to your mail");
			}
			else 
			{
				boolean passwordMatches = bCryptPasswordEncoder.matches(user.getPassword(), foundUser.getPassword());
				
				if (passwordMatches) 
				{
					loginResponse.setIsAuthenticated(true);
					loginResponse.setStatus(true);
					loginResponse.setMessage("User authenticated successfully");
					// res.addHeader(SecurityUtils.HEADER_STRING,
					// SecurityUtils.TOKEN_PREFIX + " " +
					// SecurityUtils.generateToken(foundUser.getUserName()));
					String token = SecurityUtils.TOKEN_PREFIX + " " + SecurityUtils.generateToken(foundUser.getUserName());
					loginResponse.setJwtToken(token);
					loginResponse.setUserName(foundUser.getUserName());

				} 
				else 
				{
					loginResponse.setIsAuthenticated(false);
					loginResponse.setStatus(false);
					loginResponse.setMessage("Incorrect password provided");

				}				
			}
			
		}
		return loginResponse;
	}

	@RequestMapping("/edit")
	public GetDetailsResponse getEditDetails(@RequestParam("emailAddress") String emailAddress) 
	{
		AppUser appUser = this.userService.getUserDetails(emailAddress);
		
		GetDetailsResponse res = new GetDetailsResponse();
		
		if (appUser != null) 
		{
			res.setStatus(true);
			res.setMessage("Able to get details of company and designation");
			res.setCompany(appUser.getCompany());
			res.setDesignation(appUser.getDesignation());
		} 
		else 
		{
			res.setMessage("Not able to get details of company and designation");
			res.setStatus(false);
		}

		return res;
	}

	@PostMapping("/update")
	public BaseMessage updateUserDetails(@ModelAttribute RegistrationForm user) 
	{
		return this.userService.updateUserDetails(user);
	}

	@PostMapping("/change")
	public BaseMessage changeUserPassword(@RequestBody UpdatePasswordRequest user) 
	{
		return this.userService.updateUserPassword(user);
	}

	@PostMapping("/postjobs")
	public BaseMessage postJobs(@RequestBody PostJobsRequest jobs) 
	{
		return this.userService.postJobs(jobs);
	}

	@RequestMapping("/getjobs")
	public List<GetJobsResponse> getJobs(@RequestParam("emailAddress") String emailAddress) 
	{
		return this.userService.getJobs(emailAddress);
	}

	@RequestMapping("/job")
	public GetJobsResponse getJob(@RequestParam("emailAddress") String emailAddress)
	{
		return this.userService.getJob(emailAddress);
	}

	@RequestMapping("/confirm")
	public String activateUserAccount(@RequestParam("emailAddress") String emailAddress,
			@RequestParam("confirmationToken") String confirmationToken) 
	{
		return this.userService.activateUserAccount(emailAddress, confirmationToken);

	}

//	@RequestMapping("/sendmail")
//	public void testM() {
//		this.userService.sendtestm();
//
//	}
	
	@PostMapping("/filterjobs")
	public List<GetJobsResponse> filterJobs(@RequestBody FilterJobsRequest filterJobs) 
	{
		return this.userService.filterJobs(filterJobs);
	}
	
	@PostMapping("/referme")
	public BaseMessage referMe(@RequestBody ReferMeRequest referMeRequest) 
	{
		return this.userService.referMe(referMeRequest);
	}
	
	@RequestMapping("/sendOTP")
	public BaseMessage sendOTP(@RequestParam("emailAddress") String emailAddress) {
		return this.userService.sendOTP(emailAddress);
	}
	
	@PostMapping("/resetpassword")
	public BaseMessage resetPassword(@RequestBody RestPasswordRequest restPasswordRequest) 
	{
		return this.userService.resetPassword(restPasswordRequest);
	}
	

}
