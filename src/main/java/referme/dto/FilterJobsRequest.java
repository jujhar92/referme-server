package referme.dto;

import java.util.ArrayList;

public class FilterJobsRequest extends BaseMessage {

	private ArrayList<String> jobLocations;
	private Integer minExp;
	private String emailAddress;
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public ArrayList<String> getJobLocations() {
		return jobLocations;
	}
	public void setJobLocations(ArrayList<String> jobLocations) {
		this.jobLocations = jobLocations;
	}
	public Integer getMinExp() {
		return minExp;
	}
	public void setMinExp(Integer minExp) {
		this.minExp = minExp;
	}	
	
}
