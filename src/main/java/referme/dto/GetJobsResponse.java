package referme.dto;

public class GetJobsResponse extends BaseMessage {
	
	private Long id;
	private String jobTitle;
	private String jobLocation;	
	private char isPositionStillOpen;
	private String requiredKeySkills;
	private String emailAddress;
	private String companyName;
	private boolean foundJob;
	private Integer minExp;
	
	public boolean isFoundJob() {
		return foundJob;
	}
	public void setFoundJob(boolean foundJob) {
		this.foundJob = foundJob;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getJobLocation() {
		return jobLocation;
	}
	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}
	public char getIsPositionStillOpen() {
		return isPositionStillOpen;
	}
	public void setIsPositionStillOpen(char isPositionStillOpen) {
		this.isPositionStillOpen = isPositionStillOpen;
	}
	public String getRequiredKeySkills() {
		return requiredKeySkills;
	}
	public void setRequiredKeySkills(String requiredKeySkills) {
		this.requiredKeySkills = requiredKeySkills;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Integer getMinExp() {
		return minExp;
	}
	public void setMinExp(Integer minExp) {
		this.minExp = minExp;
	}
	
}
