package referme.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class JobDto {
	private Long id;
	private String jobTitle;
	private String jobLocation;
	private char isPositionStillOpen;
	private String requiredKeySkills;
	private Integer minExp;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getJobLocation() {
		return jobLocation;
	}
	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}
	public char isPositionStillOpen() {
		return isPositionStillOpen;
	}
	public void setPositionStillOpen(char isPositionStillOpen) {
		this.isPositionStillOpen = isPositionStillOpen;
	}
	public String getRequiredKeySkills() {
		return requiredKeySkills;
	}
	public void setRequiredKeySkills(String requiredKeySkills) {
		this.requiredKeySkills = requiredKeySkills;
	}
	public char getIsPositionStillOpen() {
		return isPositionStillOpen;
	}
	public void setIsPositionStillOpen(char isPositionStillOpen) {
		this.isPositionStillOpen = isPositionStillOpen;
	}
	public Integer getMinExp() {
		return minExp;
	}
	public void setMinExp(Integer minExp) {
		this.minExp = minExp;
	}
	
	

}
