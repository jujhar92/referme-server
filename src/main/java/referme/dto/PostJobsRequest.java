package referme.dto;

import java.util.List;

public class PostJobsRequest {
	private String emailAddress;
	private List<JobDto> postedJobs;
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public List<JobDto> getPostedJobs() {
		return postedJobs;
	}
	public void setPostedJobs(List<JobDto> postedJobs) {
		this.postedJobs = postedJobs;
	}
	

}
