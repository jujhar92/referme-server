package referme.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

@Entity
@Table(name="USERS",schema ="JUJHAR")
public class AppUser {
	
	@Id
    @Column(name="EMAILADDRESS")
    private String emailAddress;
	
	@Column(name="USERNAME")
    private String userName;
	
	@Column(name="PASSWORD")
    private String password;
	
	@Column(name="COMPANY")
    private String company;
	
	@Column(name="DESIGNATION")
    private String designation;
	
	@Column(name="ISEMAILVERIFIED")
    private String isEmailVerified;
	
	@Column(name="CONFIRMATIONTOKEN")
    private String confirmationToken;
	
	@XmlElementWrapper(name="jobs")
    @XmlElement(name="job")
	@OneToMany(mappedBy="appUser",cascade=CascadeType.ALL,fetch=FetchType.LAZY)
	private List<Job> postedJobs;
	public AppUser() {}
	
	public AppUser(String email,String userName,String password,String company,String designation,String isEmailVerified,String confirmationToken) {
		this.emailAddress = email;
		this.userName = userName;
		this.password = password;
		this.company = company;
		this.designation = designation;
		this.isEmailVerified = isEmailVerified;
		this.confirmationToken = confirmationToken;
		
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Job> getPostedJobs() {
		return postedJobs;
	}

	public void setPostedJobs(List<Job> postedJobs) {
		this.postedJobs = postedJobs;
	}

	public String getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(String isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public String getConfirmationToken() {
		return confirmationToken;
	}

	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}
	
	
}
