package referme.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="JOB",schema ="JUJHAR")
public class Job {
	
	@SequenceGenerator(
	        sequenceName="JOB_SEQ", name = "jobseqgen"
	    )
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="EMAILADDRESS",nullable=false)
	private AppUser appUser;
	
	@Column(name="JOBTITLE")
	private String jobTitle;
	
	@Column(name="REQUIREDKEYSKILLS")
	private String requiredKeySkills;
	
	@Column(name="JOBLOCATION")
	private String jobLocation;
	
	@Column(name="ISPOSITIONSTILLOPEN")
	private char isPositionStillOpen;
	
	@Column(name="MINEXP")
	private Integer minExp;
	
	
	public Job(){
		
	}
	public Job(Long id,AppUser appUser,String jobTitle,String requiredKeySkills,String jobLocation,char isJobStillOpen,Integer minExp){
		this.id = id;
		this.appUser = appUser;
		this.jobTitle = jobTitle;
		this.requiredKeySkills = requiredKeySkills;
		this.jobLocation = jobLocation;
		this.setIsPositionStillOpen(isJobStillOpen);
		this.minExp = minExp;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public AppUser getAppUser() {
		return appUser;
	}
	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getRequiredKeySkills() {
		return requiredKeySkills;
	}
	public void setRequiredKeySkills(String requiredKeySkills) {
		this.requiredKeySkills = requiredKeySkills;
	}
	public String getJobLocation() {
		return jobLocation;
	}
	public void setJobLocation(String jobLocation) {
		this.jobLocation = jobLocation;
	}
	
	public char getIsPositionStillOpen() {
		return isPositionStillOpen;
	}
	public void setIsPositionStillOpen(char isPositionStillOpen) {
		if(isPositionStillOpen == '\u0000' || isPositionStillOpen == 'N') {
			this.isPositionStillOpen = 'N';
		}
		else {
			this.isPositionStillOpen = 'Y';	
		}
		
	}
	public Integer getMinExp() {
		return minExp;
	}
	public void setMinExp(Integer minExp) {
		this.minExp = minExp;
	}
	

}
