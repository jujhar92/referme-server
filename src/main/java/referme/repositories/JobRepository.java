package referme.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import referme.model.Job;

@Repository
public interface JobRepository extends CrudRepository<Job, Long> {
	
//	@Query("SELECT u FROM Job u WHERE LOWER(u.EMAILADDRESS) != LOWER(:emailAddress)")
//	List<Job> findJobs( @Param("emailAddress") String emailAddress);

}
