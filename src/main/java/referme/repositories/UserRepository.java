package referme.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import referme.dto.FilterJobsRequest;
import referme.model.AppUser;

@Repository
public interface UserRepository extends CrudRepository<AppUser,Integer>,JpaSpecificationExecutor<AppUser> {
	
	@Query("SELECT u FROM AppUser u WHERE LOWER(u.emailAddress) = LOWER(:emailAddress) and u.password = :password")
	AppUser validateUser( @Param("emailAddress") String emailAddress,@Param("password") String password);
	
	@Query("SELECT u FROM AppUser u WHERE LOWER(u.userName) = LOWER(:userName)")
	AppUser findByUsername( @Param("userName") String userName);
	
	@Query("SELECT u FROM AppUser u WHERE LOWER(u.emailAddress) = LOWER(:emailAddress)")
	AppUser findByEmailAddress( @Param("emailAddress") String emailAddress);
	
	@Transactional
	@Modifying
	@Query("Update AppUser set company = :company,designation=:designation WHERE LOWER(emailAddress) = LOWER(:emailAddress)")
	void updateUserDetails( @Param("designation") String designation,@Param("company") String company,@Param("emailAddress") String emailAddress);
	
	@Transactional
	@Modifying
	@Query("Update AppUser set password = :password WHERE LOWER(emailAddress) = LOWER(:emailAddress)")
	void updateUserPassword( @Param("password") String password,@Param("emailAddress") String emailAddress);
	
	@Query("SELECT u FROM AppUser u WHERE LOWER(u.emailAddress) != LOWER(:emailAddress)")
	List<AppUser> findJobs( @Param("emailAddress") String emailAddress);
	
	@Query("SELECT u FROM AppUser u WHERE LOWER(u.emailAddress) = LOWER(:emailAddress)")
	AppUser findJob( @Param("emailAddress") String emailAddress);
	
	@Query("SELECT u FROM AppUser u WHERE LOWER(u.emailAddress) = LOWER(:emailAddress) and u.confirmationToken = :confirmationToken")
	AppUser findUserByEmailAndToken( @Param("emailAddress") String emailAddress,@Param("confirmationToken") String confirmationToken);
}
