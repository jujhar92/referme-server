package referme.security;
import java.io.File;
import java.util.Date;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class SecurityUtils {
	
	public static final String SECRET = "SecretKeyToGenJWTs";
	public static final long EXPIRATION_TIME = 864_000_000; // 10 days
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String SIGN_UP_URL = "/users/sign-up";
	public static final String SEND_OTP_URL = "/users/sendOTP";
	public static final String REST_PASSWORD_URL = "/users/resetpassword";
	public static final String FILTER_JOBS_URL = "/users/filterjobs";
	
	public static final String JOB_LIST_URL = "/users/getjobs";
	public static final String LOGIN_URL = "/users/login";
	public static final String CONFIRMATION_URL = "/users/confirm";
	public static final String UPLOADED_FOLDER = "D:"+File.separator+"resumes"+File.separator;
	public static final String APP_URL = "http://localhost:8080/users";
	public static String generateToken(String username) {
		return Jwts.builder()
				.setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
				.compact();
	}

}
