package referme.service;

import static java.util.Collections.emptyList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import referme.dto.BaseMessage;
import referme.dto.FilterJobsRequest;
import referme.dto.GetJobsResponse;
import referme.dto.JobDto;
import referme.dto.PostJobsRequest;
import referme.dto.ReferMeRequest;
import referme.dto.RegistrationForm;
import referme.dto.RestPasswordRequest;
import referme.dto.UpdatePasswordRequest;
import referme.model.AppUser;
import referme.model.Job;
import referme.repositories.UserRepository;
import referme.security.SecurityUtils;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EmailService emailService;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public List<AppUser> getUserList() {
		List<AppUser> users = new ArrayList<AppUser>();
		this.userRepository.findAll().forEach(users::add);
		return users;
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		AppUser applicationUser = userRepository.findByUsername(userName);
		if (applicationUser == null) {
			throw new UsernameNotFoundException(userName);
		}
		return new User(applicationUser.getUserName(), applicationUser.getPassword(), emptyList());
	}

	public AppUser getUserDetails(String email) {
		return this.userRepository.findByEmailAddress(email);
	}

	public BaseMessage updateUserDetails(RegistrationForm appUser) 
	{
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setStatus(false);
		
		if (appUser.getCompany() == null) 
		{
			baseMessage.setMessage("Please provide name of company");
		} 
		else if (appUser.getDesignation() == null) 
		{
			baseMessage.setMessage("Please provide your designation");
		} 
		else 
		{
			this.userRepository.updateUserDetails(appUser.getDesignation(), appUser.getCompany(),
					appUser.getEmailAddress());
			
			if (appUser.getResume() != null) 
			{
				this.uploadResume(appUser.getResume(), appUser.getEmailAddress());
			}
			
			baseMessage.setStatus(true);
			baseMessage.setMessage("Data updated successfully");
		}
		
		return baseMessage;

	}

	public BaseMessage updateUserPassword(UpdatePasswordRequest appUser) 
	{
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setStatus(false);
		if (appUser.getNewPassword() == null) 
		{
			baseMessage.setMessage("please provide new password");
		} 
		else if (appUser.getConfirmNewPassword() == null) 
		{
			baseMessage.setMessage("confirm password is empty");
		} 
		else if (!appUser.getConfirmNewPassword().equals(appUser.getNewPassword())) 
		{
			baseMessage.setMessage("confirm password doesn't match with provided new password");
		} 
		else if (appUser.getCurrentPassword() == null) 
		{
			baseMessage.setMessage("current password is not provided");
		} 
		else 
		{
			AppUser foundUser = this.userRepository.findByEmailAddress(appUser.getEmailAddress());
			boolean passwordMatches = bCryptPasswordEncoder.matches(appUser.getCurrentPassword(),
					foundUser.getPassword());
			
			if (!passwordMatches) 
			{
				baseMessage.setMessage("Current password is not correct");
			} 
			else 
			{
				this.userRepository.updateUserPassword(bCryptPasswordEncoder.encode(appUser.getNewPassword()),
						appUser.getEmailAddress());
				
				baseMessage.setStatus(true);
				baseMessage.setMessage("Password updated successfully");
			}
		}
		return baseMessage;
	}

	public BaseMessage signUp(RegistrationForm appUser) 
	{
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setStatus(false);
		if (appUser.getUserName() == null) 
		{
			baseMessage.setMessage("Please provide your name");
		} 
		else if (appUser.getEmailAddress() == null) 
		{
			baseMessage.setMessage("Please provide email address");
		} 
		else if (appUser.getCompany() == null) 
		{
			baseMessage.setMessage("Please provide name of your organisation");
		} 
		else if (appUser.getDesignation() == null) 
		{
			baseMessage.setMessage("Please provide your designation in provide organisation");
		} 
		else if (appUser.getPassword() == null) 
		{
			baseMessage.setMessage("Please enter password, it is mandatory to create account for you");
		} 
		else 
		{
			AppUser foundUser = this.userRepository.findByEmailAddress(appUser.getEmailAddress());
			
			if (foundUser != null && "Y".equalsIgnoreCase(foundUser.getIsEmailVerified())) 
			{
				baseMessage.setMessage("You have already a accound with provided email: " + appUser.getEmailAddress());
			} 
			else 
			{
				String encryptedPassword = bCryptPasswordEncoder.encode(appUser.getPassword());
				
				String randomToken = UUID.randomUUID().toString();
				
				AppUser registeringUser = new AppUser(appUser.getEmailAddress(), appUser.getUserName(),
						encryptedPassword, appUser.getCompany(), appUser.getDesignation(), "N", randomToken);
				
				this.userRepository.save(registeringUser);
				
				this.emailService.sendConfirmationEmail(appUser.getEmailAddress(), randomToken);
				
				if (appUser.getResume() != null) 
				{
					BaseMessage baseMessage2 = uploadResume(appUser.getResume(), appUser.getEmailAddress());
					
					if (!baseMessage2.getStatus()) 
					{
						baseMessage.setMessage(
								"Account created successfully! For confirmation click on url link sent to your registered email! "
										+ baseMessage2.getMessage());
					} 
					else 
					{
						baseMessage.setMessage(
								"Account created successfully! For confirmation click on url link sent to your registered email! Resume uploaded successfully!");
					}
				} 
				else 
				{
					baseMessage.setMessage(
							"Account created successfully! For confirmation click on url link sent to your registered email! please upload resume by editing profile");
				}

				baseMessage.setStatus(true);
			}
		}
		return baseMessage;
	}

	private BaseMessage uploadResume(MultipartFile resume, String emailAddress) 
	{
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setStatus(true);
		baseMessage.setMessage("Successfully uploaded resume!");
		
		File directory = new File(SecurityUtils.UPLOADED_FOLDER);

		if (!directory.exists()) 
		{
			if (!directory.mkdirs()) 
			{
				baseMessage.setMessage("Not able to upload resume. Please try after some time");
				baseMessage.setStatus(false);
				return baseMessage;
			}
		}
		FileOutputStream fos = null;
		
		File resumeFile = new File(SecurityUtils.UPLOADED_FOLDER + File.separator + emailAddress + ".pdf");
		try 
		{
			fos = new FileOutputStream(resumeFile,false); 
			fos.write(resume.getBytes());

			//resume.transferTo(resumeFile);
		} 
		catch (IllegalStateException e) 
		{
			baseMessage.setMessage("Not able to upload resume. Please try after some time");
			baseMessage.setStatus(false);
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			baseMessage.setMessage("Not able to upload resume. Please try after some time");
			baseMessage.setStatus(false);
			e.printStackTrace();
		}
		finally {
			if(fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		return baseMessage;

	}

	public BaseMessage postJobs(PostJobsRequest jobs) {
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setStatus(false);
		if (jobs.getEmailAddress() == null) 
		{
			baseMessage.setMessage("Please register to post");
		} 
		else if (jobs.getPostedJobs() == null || jobs.getPostedJobs().size() == 0) 
		{
			baseMessage.setMessage("There is no jobs to post, please add jobs");
		} 
		else
		{
			AppUser user = userRepository.findByEmailAddress(jobs.getEmailAddress());
			
			if (user == null) 
			{
				baseMessage.setMessage("There is no registered user with provided email: " + jobs.getEmailAddress());
			} 
			else 
			{
				for (JobDto job : jobs.getPostedJobs()) 
				{
					if (job.getJobLocation() == null) 
					{
						baseMessage.setMessage("Please add job location");
						baseMessage.setStatus(false);
						continue;
					} 
					else if (job.getJobTitle() == null) 
					{
						baseMessage.setMessage("Please add job title");
						baseMessage.setStatus(false);
						continue;
					} 
					else if (job.getRequiredKeySkills() == null) 
					{
						baseMessage.setMessage("Please add required key skills for job");
						baseMessage.setStatus(false);
						continue;
					} 
					else 
					{
						Job jobModal = new Job(job.getId(), user, job.getJobTitle(), job.getRequiredKeySkills(),
								job.getJobLocation(), job.isPositionStillOpen(),job.getMinExp());
						if (user.getPostedJobs() == null) {
							user.setPostedJobs(new ArrayList<Job>());
						}
						user.getPostedJobs().add(jobModal);
						baseMessage.setStatus(true);
					}
				}

			}
			if (baseMessage.getStatus()) {
				userRepository.save(user);
				baseMessage.setMessage("Posted jobs successfully!");
			}
		}
		return baseMessage;
	}

	public List<GetJobsResponse> getJobs(String emailAddress) {
		List<GetJobsResponse> jobs = new ArrayList<GetJobsResponse>();
		List<AppUser> users = this.userRepository.findJobs(emailAddress);
		for (AppUser user : users) {
			List<Job> postedJobs = user.getPostedJobs();
			for (Job postedJob : postedJobs) {
				if('N' == postedJob.getIsPositionStillOpen() || '\u0000' == postedJob.getIsPositionStillOpen()) {
					continue;
				}
				GetJobsResponse job = new GetJobsResponse();
				job.setId(postedJob.getId());
				job.setEmailAddress(user.getEmailAddress());
				job.setIsPositionStillOpen(postedJob.getIsPositionStillOpen());
				job.setJobLocation(postedJob.getJobLocation());
				job.setJobTitle(postedJob.getJobTitle());
				job.setMinExp(postedJob.getMinExp());
				job.setRequiredKeySkills(postedJob.getRequiredKeySkills());
				job.setCompanyName(user.getCompany());
				job.setFoundJob(true);
				jobs.add(job);
			}

		}
		return jobs;
	}
	
	private String generateRestPassword() {
		int len = 8;
		String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
                String symbols = "!@#$%^&*_=+-/.?<>)";
 
 
        String values = Capital_chars + Small_chars +
                        numbers + symbols;
 
        // Using random method
        Random rndm_method = new Random();
 
        char[] password = new char[len];
 
        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] =
              values.charAt(rndm_method.nextInt(values.length()));
        }
        return password.toString();
	}
	public BaseMessage resetPassword(RestPasswordRequest restPasswordRequest) {
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setMessage("Password reset !");
		baseMessage.setStatus(false);
		if(restPasswordRequest.getNewPassword() == null) {
			baseMessage.setMessage("Please provide new password");
		}
		else if(restPasswordRequest.getEmailAddress() == null) {
			baseMessage.setMessage("Please provide email address");
		}
		else if(restPasswordRequest.getOtp() == null) {
			baseMessage.setMessage("Please provide OTP sent to your registered Email");
		}
		AppUser appUser = this.userRepository.findByEmailAddress(restPasswordRequest.getEmailAddress());
		if(appUser == null) {
			baseMessage.setMessage("Provided email address is not registered !");
		}
		else {
			if(restPasswordRequest.getOtp().equals(appUser.getConfirmationToken())) {
				String encryptedPassword = bCryptPasswordEncoder.encode(restPasswordRequest.getNewPassword());
				appUser.setPassword(encryptedPassword);
				this.userRepository.save(appUser);
				baseMessage.setStatus(true);
			}
			else {
				baseMessage.setMessage("Invalid OTP");
			}
			
		}
		
		
		return baseMessage;
	}
	public BaseMessage sendOTP(String emailAddress) {
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setMessage("Sent OTP to your registered email");
		baseMessage.setStatus(true);
		AppUser appUser = this.userRepository.findByEmailAddress(emailAddress);
		if(appUser == null) {
			baseMessage.setMessage("Provided email address is not registered !");
			baseMessage.setStatus(false);
		}
		else {
			String otp = generateOTP();
			appUser.setConfirmationToken(otp);
			this.userRepository.save(appUser);
			this.emailService.sendSimpleMessage("Your OTP:"+otp, appUser.getEmailAddress());
		}		
		return baseMessage;
	}
	public String generateOTP() {
		int len = 4;
		// Using numeric values
        String numbers = "0123456789";
 
        // Using random method
        Random rndm_method = new Random();
 
        char[] otp = new char[len];
 
        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            otp[i] =
             numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return otp.toString();
	}
	public List<GetJobsResponse> filterJobs(FilterJobsRequest filterJobs) {
		List<GetJobsResponse> jobs = new ArrayList<GetJobsResponse>();
		List<AppUser> users = this.userRepository.findAll(new Specification<AppUser>() {

			
			public Predicate toPredicate(Root<AppUser> root, CriteriaQuery< ?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();
				boolean isAll = false;
				for(String jobLocation:filterJobs.getJobLocations()) {
					if("all".equalsIgnoreCase(jobLocation)) {
						isAll = true;
						break;
					}
					predicates.add(cb.equal(root.join("postedJobs").get("jobLocation"), jobLocation));	
				}
				if(isAll) {
					predicates = new ArrayList<>();
					predicates.add(cb.notEqual(root.join("postedJobs").get("jobLocation"), " "));
				}
				if(filterJobs.getMinExp() != null) {
					List<Predicate> predicatesWithMin = new ArrayList<>();
					predicatesWithMin.add(cb.or(predicates.toArray(new Predicate[0])));
					predicatesWithMin.add(cb.greaterThanOrEqualTo(root.join("postedJobs").get("minExp"), filterJobs.getMinExp()));
					return cb.and(predicatesWithMin.toArray(new Predicate[0]));
					
				}
				else {
					return cb.or(predicates.toArray(new Predicate[0]));	
				}
//				if (filter.getDesignation() != null) {
//					predicates.add(cb.equal(root.get("designation"), filter.getDesignation()));
//				}
//
//				
//				if (filter.getFirstName() != null) {
//					pr.add(cb.like(cb.lower(root.get("firstName")), 
//                                                    "%" + filter.getFirstName().toLowerCase() + "%"));
//				}
//
//				
//				if (filter.getLastName() != null) {
//					pr.add(cb.like(cb.lower(root.get("lastName")), 
//                                                    "%" + filter.getLastName().toLowerCase() + "%"));
//				}

				
			}
		});
		
		
		
		
		
//		List<GetJobsResponse> jobs = new ArrayList<GetJobsResponse>();
//		StringBuilder whereClause = new StringBuilder();
//		boolean isAll = false;
//		for(String jobLocation:filterJobs.getJobLocations()) {
//			if(jobLocation.equalsIgnoreCase("all")) {
//				whereClause = new StringBuilder();
//				isAll = true;
//				whereClause.append("WHERE LOWER(jobLocation) NOT NULL");
//				break;
//			}
//			if(whereClause.length()>0) {
//				whereClause.append(jobLocation.toLowerCase()+",");
//			}
//			else {
//				whereClause.append("WHERE LOWER(jobLocation) IN ("+jobLocation.toLowerCase()+",");
//			}
//		}
//		if(!isAll) {
//			whereClause.deleteCharAt(whereClause.length()-1);
//			whereClause.append(")");
//		}
//		
//		if(filterJobs.getMinExp() != null) {
//			if(whereClause.length() >0) {
//				whereClause.append(" and minExp < "+filterJobs.getMinExp());
//			}
//			else {
//				whereClause.append("where minExp < "+filterJobs.getMinExp());
//			}
//		}
		
		
		//List<AppUser> users = this.userRepository.findFilteredJobs(filterJobs);
		for (AppUser user : users) {
			List<Job> postedJobs = user.getPostedJobs();
			for (Job postedJob : postedJobs) {
				if('N' == postedJob.getIsPositionStillOpen() || '\u0000' == postedJob.getIsPositionStillOpen()) {
					continue;
				}
				if(filterJobs.getEmailAddress() != null) {
					if(user.getEmailAddress().equals(filterJobs.getEmailAddress())) {
						continue;
					}
				}
				GetJobsResponse job = new GetJobsResponse();
				job.setId(postedJob.getId());
				job.setEmailAddress(user.getEmailAddress());
				job.setIsPositionStillOpen(postedJob.getIsPositionStillOpen());
				job.setJobLocation(postedJob.getJobLocation());
				job.setJobTitle(postedJob.getJobTitle());
				job.setMinExp(postedJob.getMinExp());
				job.setRequiredKeySkills(postedJob.getRequiredKeySkills());
				job.setCompanyName(user.getCompany());
				job.setFoundJob(true);
				jobs.add(job);
			}

		}
		return jobs;
	}

	public GetJobsResponse getJob(String emailAddress) {
		GetJobsResponse job = new GetJobsResponse();
		job.setStatus(true);
		job.setFoundJob(false);
		AppUser user = this.userRepository.findJob(emailAddress);
		if (user != null) {
			List<Job> postedJobs = user.getPostedJobs();
			if (postedJobs != null && postedJobs.size() > 0) {
				Job postedJob = postedJobs.get(0);
				job.setId(postedJob.getId());
				job.setEmailAddress(user.getEmailAddress());
				job.setIsPositionStillOpen(postedJob.getIsPositionStillOpen());
				job.setJobLocation(postedJob.getJobLocation());
				job.setJobTitle(postedJob.getJobTitle());
				job.setMinExp(postedJob.getMinExp());
				job.setRequiredKeySkills(postedJob.getRequiredKeySkills());
				job.setCompanyName(user.getCompany());
				job.setFoundJob(true);
				job.setMessage("Successfully able to get posted job details");
			} else {
				job.setMessage("Not able find posted job, please post job");
			}
		} else {
			job.setStatus(false);
			job.setMessage("Unauthorised user");

		}

		return job;
	}

	public String activateUserAccount(String emailAddress, String confirmationToken) {
		AppUser user = this.userRepository.findUserByEmailAndToken(emailAddress, confirmationToken);

		if (user == null) 
		{
			return "Invalid token or expired! Please register again";
		} 
		else 
		{
			user.setIsEmailVerified("Y");
			this.userRepository.save(user);
			return "Your account activated!";
		}
	}

	public void sendtestm() 
	{
		//EmailSender.send("refermebro@gmail.com", "maneetsingh", "junjar.singh92@gmail.com", "test", "testmail");
		this.emailService.sendConfirmationEmail("jujhar.singh92@gmail.com", "test");
	}
	
	public BaseMessage referMe(ReferMeRequest referMeRequest) {
		BaseMessage baseMessage = new BaseMessage();
		baseMessage.setMessage("Sent referral successfully !");
		baseMessage.setStatus(true);
		this.emailService.sendReferalMail(referMeRequest.getToEmailAddress(), referMeRequest.getFromEmailAddress(), referMeRequest.getJobPosition());
		return baseMessage;
	}

}
